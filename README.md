## README

Ten kod to program w języku C++, który ma za zadanie zweryfikować możliwości człowieka do przeczytania tekstu z poprzestawianymi literami. Test odbył się na postawie inwokacji Adama Mickiewicza. Tak długo jak pierwsza i ostatnia litera są na swoich miejscach odczytanie tekstu jest łatwe. Trudności zaczynają się po zupełnym przemieszaniu liter.

### Kompilacja i Uruchomienie
Aby skompilować ten kod, potrzebujesz kompilatora C++ (np. GNU GCC Compiler) oraz środowiska programistycznego (IDE) takiego jak CodeBlocks 17.12. Postępuj zgodnie z poniższymi krokami, aby skompilować i uruchomić program:

1. Skonfiguruj środowisko programistyczne z CodeBlocks i GNU GCC Compiler.
2. Utwórz nowy projekt C++ w CodeBlocks.
3. Skopiuj kod do pliku źródłowego projektu (np. `main.cpp`).
4. Zapisz kod i zbuduj projekt.
5. Upewnij się, że plik wejściowy "tekst.txt" znajduje się w tym samym katalogu co skompilowany plik wykonywalny.
6. Uruchom program, a zmiany zostaną wprowadzone do pliku tekstowego.

### Funkcjonalność
Program udostępnia następującą funkcjonalność do modyfikacji pliku tekstowego:

1. **Zamiana liter wewnątrz wyrazów**: Program losowo wybiera dwie litery wewnątrz wyrazu i zamienia ich pozycje. Pierwsza i ostatnia litera w każdym wyrazie pozostają niezmienione. Ta operacja jest wybierana przez wpisanie "1" po zapytaniu.
2. **Zamiana miejscami pierwszej i ostatniej litery w każdym wyrazie**: Program zamienia miejscami pierwszą i ostatnią literę w każdym wyrazie, pozostawiając niezmienione litery środkowe. Ta operacja jest wybierana przez wpisanie "2" po zapytaniu.
3. **Brak modyfikacji**: Program nie wykonuje żadnych zmian w pliku tekstowym. Ta opcja jest wybierana przez wpisanie "3" po zapytaniu.

### Obsługa plików
Program otwiera plik "tekst.txt" w trybie dołączania (append), odczytuje każdą linię, wykonuje wybrane modyfikacje na linii i zapisuje zmodyfikowaną linię z powrotem do pliku. Program kontynuuje ten proces, aż osiągnie koniec pliku.

### Wejście i Wyjście
Program prosi użytkownika o wybór operacji, która ma zostać wykonana na pliku tekstowym. Dostępne opcje są wyświetlane w następujący sposób:

```
Wybierz co chcesz zrobić
1. Zamiana liter wewnątrz wyrazu bez zmiany pierwszych i ostatnich liter
2. Zamiana miejscami pierwszej i ostatniej litery
3. Nic nie rób (koniec programu)
```

Po dokonaniu modyfikacji program albo dopisuje zmodyfikowany tekst z odpowiednim nagłówkiem ("Tekst po zamianie 1:" lub "Tekst po zamianie 2:"), albo kończy działanie, jeśli nie dokonano żadnych zmian.

### Obsługa błędów
Program obsługuje następujące błędy:

- Jeśli plik wejściowy "tekst.txt" nie zostanie odnaleziony lub nie można go otworzyć, program kończy działanie i wyświetla komunikat o błędzie.
- Jeśli podano nieprawidłowe dane wejściowe po zapytaniu o wybraną operację, program wyświetla komunikat o błędzie i prosi o podanie danych ponownie.

### Ograniczenia
- Program zakłada, że plik wejściowy "tekst.txt" znajduje się w tym samym katalogu co skompilowany plik wykonywalny.
