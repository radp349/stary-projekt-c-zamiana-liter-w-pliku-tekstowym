// POP 2019-01-16 projekt 2 Pawelec Radosław EIT 2 175631       CodeBlocks 17.12  GNU GCC COMPILER
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;


void Zamiana_rozmiaru(char &a,char &b) //jezeli litery ktore maja byc zamieniowe sa roznej wielkosci, to mala staje sie duza, a duza mala
{ bool A=1,B=1,X;
char x;
int Mpolskie_znaki[8]= {-71,-26,-22,-77,-13,-100,-97,-65}, Dpolskie_znaki[8]={-91,-58,-54,-93,-45,-116,-113,-81};

    for(int i=97;i<123;i++)
    {
        if((int)a==i){A=0;}
        if((int)b==i){B=0;}
    }

    for(int i=0;i<8;i++)
    {
        if((int)a==Mpolskie_znaki[i]){A=0;}
        if((int)b==Mpolskie_znaki[i]){B=0;}
    }

    if(A!=B)
        {for(int i=0;i<2;i++)
                {
                                if(i==0){x=a; X=A;}
                                else if(i==1){x=b; X=B;}

                                if(((int)x>0)&&(X==1)){x=(char)((int)x+32);}
                                else if(((int)x>0)&&(X==0)){x=(char)((int)x-32);}
                                else if(((int)x<0)&&(X==1))
                                    {
                                        for(int j=0;j<8;j++)
                                            {
                                                    if((int)x==Mpolskie_znaki[j]){x=(char)Dpolskie_znaki[j];}
                                                    else if((int)x==Dpolskie_znaki[j]){x=(char)Mpolskie_znaki[j];}
                                            }
                                    }
                                if(i==0){a=x; A=X;}
                                else if(i==1){b=x; B=X;}
                }
        }
}


string Zamiana_liter_1(string wyraz, int jeden,int dwa)  //funkcja zamienia ze soba, wskazane przez losowanie znaki
{
    char a,b;

    a=wyraz[jeden];
    b=wyraz[dwa];

    Zamiana_rozmiaru(a,b);

    wyraz[jeden]=b;
    wyraz[dwa]=a;

    return wyraz;
}



string Zamiana_liter_2(string wyraz) //funkcja zamienia ze soba pierwsza i ostatnia litere
{
    char a,b;
    a=wyraz[0];
    b=wyraz[wyraz.length()-1];


    Zamiana_rozmiaru(a,b);

    wyraz[0]=b;
    wyraz[wyraz.length()-1]=a;

    return wyraz;
}


bool czy_litera(string alfanumeryczny)  //funkcja sprawdza, czy znak jest litera uzywana w polskim alfabecie
{ int Polskieznaki[16]={-71,-26,-22,-77,-13,-100,-97,-65,-91,-58,-54,-93,-45,-116,-113,-81};

    if(((int)alfanumeryczny[0]>64)&&((int)alfanumeryczny[0]<91)) {return 1;}
    else if(((int)alfanumeryczny[0]>96)&&((int)alfanumeryczny[0]<123)) {return 1;}
    else{
            for(int i=0;i<16;i++)
        {
            if((int)alfanumeryczny[0]==Polskieznaki[i]){return 1;}
        }}

return 0;
}


string Losowanie(string verbum)  //funkcja wybiera losowo ktore znakiw slowie, zostana zamienione
{int fate1,fate2,pastfate1=0,pastfate2=0,zmiany;
    zmiany=verbum.length()-3; //dla slowa o dlugosci 5, dokonamy dwoch zamian 5-3=2
    srand(time(NULL));

    for(int i=0;i<zmiany;i++)
    {
        fate1=rand()%(verbum.length()-2)+1;
        fate2=rand()%(verbum.length()-2)+1;

    while((fate1==fate2)||((fate1==pastfate2)&&(fate2==pastfate1))||((fate1==pastfate1)&&(fate2==pastfate2))) //upewniamy sie, ze niedokonamy dwoch odwrotnych zamian pod rzad
        {fate1=rand()%(verbum.length()-2)+1;
        fate2=rand()%(verbum.length()-2)+1;
        }

            if(fate1!=fate2){verbum=Zamiana_liter_1(verbum,fate1,fate2);}

        pastfate1=fate1;
        pastfate2=fate2;
}

return verbum;}



void Podziel_na_slowa(string &zdanie,int numer) //funkcja tablicuje pobrana przez getline linijke, oddzielajac wyrazy od znakow interpunkcyjnych
{int ile_komorek=1,j=0;
string current,next;

    for(int i=0;i<zdanie.length()-1;i++)  //sprawdzamy jak wiele komorek w tablicy potrzebujemy
        {current=zdanie[i];
        next=zdanie[i+1];
            if(czy_litera(current)!=czy_litera(next)) {ile_komorek++;}
            else if(czy_litera(current)==0){ile_komorek++;}
        }

    string Slowa[ile_komorek];

    for(int i=0;i<zdanie.length();i++)
        {Slowa[j]+=zdanie[i];
            current=zdanie[i];
            next=zdanie[i+1];

            if(czy_litera(current)!=czy_litera(next)) {j++;}
            else if (czy_litera(current)==0) {j++;}
        }

    zdanie.clear();
    for(int i=0;i<ile_komorek;i++)  //konkretne wyrazy wysylamy do funkcji dokonujacej zamiany
    {

       if((Slowa[i].length()>3)&&(numer==1)) {zdanie+=Losowanie(Slowa[i]);}
       else if((Slowa[i].length()>1)&&(numer==2)) {zdanie+=Zamiana_liter_2(Slowa[i]);}
       else{zdanie+=Slowa[i];}
    }



}

void Co_robimy(string &zmienna)
{cout<<"Wybierz co chcesz zrobic"<<endl;
 cout<<"1. Zamiana liter wewnatrz wyrazu bez zmiany pierwszych i ostatnich liter"<<endl;
 cout<<"2. Zamiana miejscami pierwszej i ostatniej litery"<<endl;
 cout<<"3. Nic nie rob (koniec programu)"<<endl;
    while ((zmienna!="1")&&(zmienna!="2")&&(zmienna!="3"))
        {
            cin>>zmienna;
            if((zmienna!="1")&&(zmienna!="2")&&(zmienna!="3")) {cout<<"Podano niepoprawne dane"<<endl;}
        }


}

string Manager(int opcja, string linia) //funkcja podejmuje dzialanie, w oparciu o wybor uzytkownika
{

   if((opcja==1)&&(linia.length()>3)) {Podziel_na_slowa(linia,opcja);}

    if((opcja==2)&&(linia.length()>1)){Podziel_na_slowa(linia,opcja);}

    if (opcja==3){return "0";}

return linia;
}

int Otwarcie() //funkcja otwiera plik, pobiera linijke, wysyla ja do innych funkcji w celu zmodyfikowania i zapisuje z powrotem do pliku
{   int znak=0;
    fstream plik;
    string wariant="wartosc stratowa", po_zamianie,linijka="wartosc startowa";
    int wybor;

        plik.open("tekst.txt",ios::in | ios::app);
            if (plik.good()==false){return 0;} //otwieramy plik i sprawdzamy czy zostal poprawnie otworzony

        Co_robimy(wariant);  //pytamy uzytkownika co zrobic z plikiem
        wybor=atoi(wariant.c_str());

    if(wybor==1){plik<<endl;
    plik<<"Tekst po zamianie 1:"<<endl;}
                                                /*dopisujemy tresc na koncu pliku*/
    else if(wybor==2){plik<<endl;
    plik<<"Tekst po zamianie 2:"<<endl;}

    plik.seekg(znak,ios::beg); //ustawiamy na poczatek pliku

            while(getline(plik,linijka))
                   {znak=znak+2+linijka.length(); //ignorujemy wczesniej zmienione linijki

                    if((linijka=="Tekst po zamianie 1:")||(linijka=="Tekst po zamianie 2:")) //dojscie do tych linijek oznacza, ze caly tekst zostalprzerobiony
                        {plik.close();
                            return 3;}

                    po_zamianie=Manager(wybor,linijka);
                    if(po_zamianie=="0")
                        {return 2;
                        plik.close();}
                    plik<<po_zamianie<<endl;   //dopisujemy zmieniona linijke na koncu progrmu
                    plik.seekg(znak,ios::beg);}


return 1;
}

void Wypisanie() //Funkcja wypisuje plik na ekran
{fstream plik;
 plik.open("tekst.txt",ios::in);
string linijka;

while(getline(plik,linijka))
                   {cout<<linijka<<endl;}
plik.close();}


int main()
{int Czy_dziala;  //Zmienna sprawdza czy plik zosta³ prawid³owo otwarty oraz wy³¹cza program na ¿yczenie u¿ytkownika

    Czy_dziala=Otwarcie();  //Wywolanie funkcji otwierajacej plik

        if(Czy_dziala==0){cout << "Plik do odczytu, nie istnieje!" << endl; //Koñczy program, jezeli plik niezostal prawidlowo otwarty
        return 0;}
        else if(Czy_dziala==2){return 0;} //Wyylcza program na zyczenie u¿ytkownika

    //Wypisanie();    Opcjonalne wypiasnie zawartoœci pliku tekstowego
    return 0;
}

